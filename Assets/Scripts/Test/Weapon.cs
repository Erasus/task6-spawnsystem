﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : ObjectPool
{
    //[SerializeField] private Transform _shootPosition;
    [SerializeField] private float _offset = 180;
    [SerializeField] private Bullet _enemyTemplates;
    [SerializeField] private Bullet[] _bulletTemplates;
    [SerializeField] private WeaponType _bulletType;

    private Vector3 _target;

    public WeaponType BulletType => _bulletType;

    private void Awake()
    {
        //Initialize(_enemyTemplates);
        Initialize(_bulletTemplates);
    }

    private void Update()
    {
        /*if (Input.GetMouseButtonDown(0))
        {
            ShootWithPool(_shootPosition);
            //ShootWithInstantiate(_shootPosition);
        }*/
    }

    public void ShootWithPool(Transform shootPosition)
    {
        if (TryGetObject(out Bullet bullet, this))
        {
            _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 difference = _target - shootPosition.transform.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            shootPosition.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);
           SetBullet(bullet, shootPosition);
        }

        /*target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 difference = target - shootPosition.transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        shootPosition.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);*/

        //_bullet.gameObject.SetActive(true);
        //_bullet.transform.position = shootPosition.transform.position;
        //_bullet.transform.rotation = shootPosition.transform.rotation;
        //_bullet.SetPosition(target);
    }

    private void SetBullet(Bullet bullet, Transform transform)
    {
        bullet.gameObject.SetActive(true);
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.SetPosition(_target);
    }

    public void ShootWithInstantiate(Transform shootPosition)
    {
        Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 difference = target - shootPosition.transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        shootPosition.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);
        Bullet _bullet = Instantiate(_enemyTemplates, transform);
        SetBulletTarget(_bullet, shootPosition, target);
    }
    private void SetBulletTarget(Bullet bullet, Transform transform, Vector2 target)
    {
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.SetPosition(target);
    }
}
