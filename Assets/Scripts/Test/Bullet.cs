﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Collider2D _damageCollider;
    [SerializeField] private WeaponType _bulletType;
    [SerializeField] private int _capacity;

    private Vector3 _targetPosition;
    public WeaponType BulletType => _bulletType;
    public int Capacity => _capacity;

    private void FixedUpdate()
    {
        if (transform.position != _targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _speed * Time.deltaTime);
        }
        else
        {
            _damageCollider.enabled = true;
            StartCoroutine(Hide());
        }
    }

    public void SetPosition(Vector2 target)
    {
        _targetPosition = target;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Square square))
        {
            square.TakeDamage();
        }
    }

    private IEnumerator Hide()
    {
        yield return new WaitForEndOfFrame();
        _damageCollider.enabled = false;
        gameObject.SetActive(false);
        //Destroy(gameObject);
    }
}
