﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AmmoPool : MonoBehaviour
{
    [SerializeField] protected GameObject _shotgunContainer;

    private List<Bullet> _shotgunPool = new List<Bullet>();

    protected void Initialize(Bullet prefab)
    {
        for (int i = 0; i < prefab.Capacity; i++)
        {
            Bullet spawned = Instantiate(prefab, _shotgunContainer.transform);
            spawned.gameObject.SetActive(false);

            _shotgunPool.Add(spawned);
        }
    }

    protected void Initialize(Bullet[] prefab)
    {
        for (int j = 0; j < prefab.Length; j++)
        {
            for (int i = 0; i < prefab[j].Capacity; i++)
            {
                Bullet spawned = Instantiate(prefab[j], _shotgunContainer.transform);
                spawned.gameObject.SetActive(false);

                _shotgunPool.Add(spawned);
            }
        }
    }

    protected bool TryGetObject(out Bullet result, Weapon player)
    {
        result = _shotgunPool.FirstOrDefault(p => p.gameObject.activeSelf == false && p.BulletType == player.BulletType);

        return result != null;
    }
}

/*public enum WeaponType
{
    AK47,
    Shotgun,
    Pistolet
}*/

