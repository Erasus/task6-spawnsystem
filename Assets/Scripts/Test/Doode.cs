﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doode : MonoBehaviour
{
    [SerializeField] private Weapon _weapon;
    [SerializeField] private Transform _shootPosition;
    [SerializeField] private Animator _animator;

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _animator.Play("Shoot");
        }
    }

    public void Shoot()
    {
        _weapon.ShootWithPool(_shootPosition);
    }
}
