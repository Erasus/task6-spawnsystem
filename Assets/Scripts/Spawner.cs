﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Enemy _enemy;
    
    private SpawnPoint[] _points;
    private float _startTimeBetweenSpawn = 2;
    private float _timeBetweenSpawn;

    private void Start()
    {
        _points = GetComponentsInChildren<SpawnPoint>();
    }

    private void Update()
    {
        if(_timeBetweenSpawn <= 0)
        {
            int rand = Random.Range(0, _points.Length);
            Instantiate(_enemy, _points[rand].transform.position, Quaternion.identity); 
            _timeBetweenSpawn = _startTimeBetweenSpawn;
        }
        else
        {
            _timeBetweenSpawn -= Time.deltaTime;
        }
    }
}
