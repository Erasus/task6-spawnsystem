﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private float _speed = 0.03f;
    private float _lifeTime = 4;
    private float _horizontal;
    private float _vertical;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.color = new Color(Random.Range(0.1f, 1), Random.Range(0.1f, 1), Random.Range(0.1f, 1));
        _horizontal = Random.Range(-1f, 1f);
        _vertical = Random.Range(-1f, 1f);

        Destroy(gameObject, _lifeTime);
    }

    private void Update()
    {
        transform.Translate(new Vector2(_horizontal, _vertical) * _speed);
    }
}
