﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickMove : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _offset;

    private Vector3 _targetPosition;
    private bool _isMooving = false;

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            SetPosition();
        }

        if(_isMooving)
        {
            Move();
        }
    }

    private void SetPosition()
    {
        _targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //float rotationZ = Mathf.Atan2(_targetPosition.y, _targetPosition.x) * Mathf.Rad2Deg;
        //transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);
        _targetPosition.z = transform.position.z;

        _isMooving = true;
    }

    private void Move()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, _targetPosition);
        transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _speed * Time.deltaTime);

        if(transform.position == _targetPosition)
        {
            _isMooving = false;
        }
    }
}
